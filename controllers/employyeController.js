const connection = require("../config/database");
const { QueryTypes } = require("sequelize");
const { sequelize, Sequelize } = require("../models");

const eom = require("../models/eom")(sequelize, Sequelize.DataTypes);
const users = require("../models/user")(sequelize, Sequelize.DataTypes);
/**
 *Controller that creates/add employee of month.
   @author:Mubariz Ali
 * @param {*} req: user_id,month,year
 * @param {*} res: sends success or failure message 
 */
const Employee_of_month = async (req, res) => {
  let userId = req.body.id;
  let month = req.body.month;
  let year = req.body.year;
  console.log(req.body.id);

  try {
    // const user = await User.findOne({ where: { id: userId } });
    //console.log(user.id);
    const employee = await eom.create({
      userid: userId,
      month: month,
      year: year,
    });
    return res.status(200).send({ message: "Employee of the month added" });
  } catch (err) {
    console.log(err);
    console.log("haha error");

    return res.status(500).json(err);
  }
  // connection.query(
  //   "insert into employee_of_month set ?",
  //   {
  //     user_id: user_id,
  //     month: month,
  //     year: year,
  //   },
  //   (error, result) => {
  //     if (error) {
  //       if (error.errno == 1452) {
  //         console.log("employee does not exist");
  //         res.send({ message: "employee does not exist" });
  //       }
  //     } else {
  //       console.log(result);
  //       res.send({ message: "Employee of the month added" });
  //     }
  //   }
  // );
};
/**
 * Controller that displays employee of the month list
 * @param {*} req
 * @param {*} res: result that contains list of employee of month
 */
const All_employee_of_month = async (req, res) => {
  const alleom = await sequelize.query(
    "SELECT users.name,eom.month,eom.year FROM users JOIN eom ON users.id = eom.userid",
    {
      type: QueryTypes.SELECT,
    }
  );
  res.status(200).json(alleom);
  console.log(JSON.stringify(alleom, null, 2));
};

// fetching employee of the month for a particular month and year
const employeemonth = async (req, res) => {
  let month = req.body.month;
  let year = req.body.year;
  try {
    const employeeMonth = await sequelize.query(
      "SELECT users.name FROM users JOIN eom ON users.id = eom.userid where eom.month= ? AND eom.year= ? ",
      {
        type: QueryTypes.SELECT,
        replacements: [month, year],
      }
    );
    res.status(200).json(employeeMonth);
    console.log(JSON.stringify(employeeMonth, null, 2));
  } catch (err) {
    res.send({
      message: "There is no employee of month for this month and year ",
    });
  }
};

module.exports = { Employee_of_month, All_employee_of_month, employeemonth };
