const connection = require("../config/database");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { sequelize, Sequelize } = require("../models");
const { QueryTypes } = require("sequelize");
const users = require("../models/user")(sequelize, Sequelize.DataTypes);

// signup/register controller
/**
 * controller used for registering users/employee
 * @author:Mubariz
 * @param {*} req :name,email,password,c_password,role
 * @param {*} res :response contain success or failure message
 */
const registerUser = async (req, res) => {
  let name = req.body.name;
  let email = req.body.email;
  let password = req.body.password;
  let role = req.body.role.toLowerCase();
  try {
    const anyuser = await users.findAll({
      where: { email: email },
    });
    if (anyuser.length > 0) {
      console.log(anyuser);
      return res.send({ message: "users already exist " });
    } else {
      let hashedpassword = await bcrypt.hash(password, 8);
      console.log(hashedpassword);
      const userresult = await users.create({
        name,
        email,
        role,
        password: hashedpassword,
      });
      res.status(200).send({ message: "users registered" });
    }
  } catch (err) {
    console.log(err);
    return res.status(500).json(err);
  }
  // connection.query(
  //   "select email from users where email=?",
  //   [email],
  //   async (error, result) => {
  //     if (error) {
  //       return res.send(error);
  //     }
  //     if (result.length > 0) {
  //       return res.send({ message: "users already exist " });
  //     }
  //     //else if (password !== c_password) {
  //     //   return res.send("Passwords do no match ");
  //     // }

  //     let hashedpassword = await bcrypt.hash(password, 8);
  //     console.log(hashedpassword);

  //     connection.query(
  //       "insert into users set ?",
  //       {
  //         name: name,
  //         email: email,
  //         password: hashedpassword,
  //         role: role,
  //       },
  //       (error, result) => {
  //         if (error) {
  //           console.log(error);
  //           res.send(error);
  //         } else {
  //           console.log(result);
  //           res.status(200).send({ message: "users registered" });
  //         }
  //       }
  //     );
  //   }
  // );
};

/**
 * This is a controller function that help a users to log into the system.
 * @author: Mubraiz
 * @param {*} req : request parameter of middleware
 * @param {*} res : token , user_id, email
 *
 */
const loginUser = async (req, res) => {
  let password = req.body.password;
  let email = req.body.email;
  let role = req.body.role;
  try {
    const anyuser = await users.findAll({
      where: { email: email },
    });
    if (anyuser.length > 0) {
      console.log("users maujood ha");
      bcrypt.compare(password, anyuser[0].password, (err, resul) => {
        console.log(resul);
        if (!resul) {
          return res.status(401).send({
            message: "password matching fail",
          });
        }

        if (resul) {
          const token = jwt.sign(
            {
              email: anyuser[0].email,
              id: anyuser[0].id,
              role: anyuser[0].role,
            },
            "this is my secret key",
            {
              expiresIn: "1h",
            }
          );
          res.status(200).json({
            email: anyuser[0].email,
            id: anyuser[0].id,
            token: token,
            role: anyuser[0].role,
          });
        }
      });
    } else {
      return res.send({ message: "users does not exist" });
    }
  } catch (err) {
    console.log("email ni mila ");
    return res.status(500).json(err);
  }
  // connection.query(
  //   "select * from users where email=?",
  //   [email],
  //   (error, result) => {
  //     if (error) {
  //       return res.send(error);
  //     }
  //     // if email is wrong it won't enter in the if body because query result will return empty array
  //     if (result.length) {
  //       bcrypt.compare(password, anyuser[0].password, (err, resul) => {
  //         console.log(resul);
  //         if (!resul) {
  //           return res.status(401).send({
  //             msg: "password matching fail",
  //           });
  //         }
  //         if (resul) {
  //           const token = jwt.sign(
  //             {
  //               email: anyuser[0].email,
  //               user_id: anyuser[0].user_id,
  //               role: anyuser[0].role,
  //             },
  //             "this is my secret key",
  //             {
  //               expiresIn: "1h",
  //             }
  //           );
  //           res.status(200).json({
  //             email: anyuser[0].email,
  //             user_id: anyuser[0].user_id,
  //             token: token,
  //             role: anyuser[0].role,
  //           });
  //         }
  //       });
  //     } else {
  //       return res.send({ message: "no users found" });
  //     }
  //   }
  // );
};

// get allusers
/**
 *controller that returns all employees of focusteck
 * @param {*} req
 * @param {*} res : result that contains employees list
 */
const allusers = async (req, res) => {
  try {
    const alluser = await sequelize.query("SELECT *  FROM users ", {
      type: QueryTypes.SELECT,
    });
    res.status(200).json(alluser);
    console.log(JSON.stringify(alluser, null, 2));
  } catch (err) {
    res.json(err);
  }
};

// showing the month and year in which user has been eom

const eom = async (req, res) => {
  let userid = req.params.id;
  try {
    const user = await sequelize.query(
      "SELECT eom.month,eom.year FROM users JOIN eom ON users.id = eom.userid where users.id= ? ",
      {
        type: QueryTypes.SELECT,
        replacements: [userid],
      }
    );
    res.status(200).json(user);
    console.log(JSON.stringify(user, null, 2));
  } catch (err) {
    console.log(err);
    res.send({ message: "this user has never been employee of the month" });
  }
};
module.exports = { registerUser, loginUser, allusers, eom };
