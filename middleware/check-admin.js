const jwt = require("jsonwebtoken");
/**
 * Middleware that checks user role is admin
 * @param {*} req :token
 * @param {*} res : returns success or failure mesage
 * @param {*} next : goes to employee controller
 *
 */
const checkAdmin = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1]; // seperating bearer from token
    const decodedToken = jwt.verify(token, "this is my secret key"); // if verify function returns false it will go to catch otherwise, it will to go to next() and next() leads us to get API

    if (decodedToken.role !== "admin") {
      return res.status(401).json({
        msg: "Only admin is allowed to add Employee of the month",
      });
    }
    next(); // if we forget to put next() it won't execute get users API
  } catch (error) {
    return res.status(401).json({
      msg: "invalid token admin",
    });
  }
};
module.exports = { checkAdmin };
