/**
 * Generalised middleware that validates all user input values
 * @param {*} body: takes another middleware
 * @param {*} keys : the values needs to be validated e.g email,passwprd
 *
 */
const finalvalidation = (body, keys) => {
  const checkinArray = (val, arr) =>
    arr.reduce((acc, curr) => acc || val === curr.atr, false);
  console.log(body);
  let validationError = [];
  keys.forEach((value) => {
    console.log(value);
    if (!body[value]) {
      validationError.push({ atr: value, msg: "Please enter " + value });
    } else {
      // email validation

      if (value == "email") {
        apos = body[value].indexOf("@");
        dotpos = body[value].lastIndexOf(".");
        if (apos < 1 || dotpos - apos < 2) {
          validationError.push({ atr: value, msg: "Invalid email" });
        }
      }

      //password validation
      if (value == "password") {
        let illegalChars = /[\W_]/; // allow only letters and numbers and password length should be greater than 5
        if (body[value].length < 6) {
          validationError.push({
            atr: value,
            msg: "Password length is short, atleast 6 letter required",
          });
        } else if (illegalChars.test(body[value])) {
          validationError.push({
            atr: value,
            msg: "Invalid password, only letters and numbers are allowed",
          });
        }
      }

      //role validation
      if (value == "role") {
        if (
          body[value].toLowerCase() !== "admin" &&
          body[value].toLowerCase() !== "user"
        ) {
          validationError.push({ atr: value, msg: "Invalid role" });
        }
      }
      // user_id validation
      if (value == "user_id") {
        if (isNaN(body[value])) {
          validationError.push({
            atr: value,
            msg: "Invalid type, only numeric is allowed fro user_id ",
          });
        }
        if (body[value] <= 0) {
          validationError.push({ atr: value, msg: "Invalid user_id " });
        }
      }

      // month validation
      if (value == "month") {
        if (isNaN(body[value])) {
          validationError.push({
            atr: value,
            msg: "Invalid type, only numeric is allowed for month",
          });
        }
        ///////
        if (!checkinArray("year", validationError)) {
          let d = new Date();
          let allowedMonth = 11;

          if (body["year"] == d.getFullYear()) {
            allowedMonth = d.getMonth();
          }
          console.log(body[value], "  ", allowedMonth);
          if (parseInt(body[value]) > allowedMonth) {
            validationError.push({
              atr: value,
              msg: "Please enter correct month(0-11)",
            });
          }
        }
      }
      // year validation
      if (value == "year") {
        if (isNaN(body[value])) {
          validationError.push({
            atr: value,
            msg: "Invalid type, only numeric is allowed for year ",
          });
        }
        let d = new Date();
        if (body[value] > d.getFullYear() || body[value] < 2010) {
          validationError.push({
            atr: value,
            msg: "Please enter correct year",
          });
        }
      }
    }
  });
  return validationError;
};

// month validation
module.exports = { finalvalidation };
