const jwt = require("jsonwebtoken");
/**
 * Login Middleware that allows only authorized users to login
 * @param {*} req :Token
 * @param {*} res : contains status and error message .
 *
 *
 */
const checkAuth = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1]; // seperating bearer from token
    const decodedToken = jwt.verify(token, "this is my secret key"); // if verify function returns false it will go to catch otherwise, it will to go to next() and next() leads us to get API

    if (!req.headers.id || req.headers.id != decodedToken.id) {
      return res.status(401).json({
        msg: "Invalid Id",
      });
    }
    next(); // if we forget to put next() it won't execute get users API
  } catch (error) {
    return res.status(401).json({
      msg: "Invalid token",
    });
  }
};
module.exports = { checkAuth };
