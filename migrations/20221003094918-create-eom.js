"use strict";

const { sequelize } = require("../models");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("eom", {
      month: {
        allowNull: false,
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      year: {
        allowNull: false,
        type: Sequelize.INTEGER,
        primaryKey: true,
      },

      userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: "user", key: "id" },
      },

      // id: {
      //   allowNull: false,
      //   type: Sequelize.INTEGER,
      // },
      // id: {
      //   type: sequelize.INTEGER,
      //   allowNull: false,
      //   references: {
      //     model: "User",
      //     key: "id",
      //   },
      //   onUpdate: "CASCADE",
      //   onDelete: "RESTRICT",
      // },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("eom");
  },
};
