"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class eom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate({ users }) {
      // define association here
      this.hasOne(users, { foreignKey: "userid" });
      // console.log("Assocaition set between EOM and USER");
    }
  }
  eom.init(
    {
      month: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      year: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      userid: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "users",
          key: "id",
        },
      },
    },
    {
      sequelize,
      modelName: "eom",
      tableName: "eom",
    }
  );

  return eom;
};
