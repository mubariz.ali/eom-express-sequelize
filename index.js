const { sequelize } = require("./models");
const express = require("express");
const path = require("path");
const app = express();
const port = 3000;
//const products = require("./routes/products"); // importing product module
const users = require("./routes/users");
const employee = require("./routes/eom");
const cors = require("cors");
const bodyParser = require("body-parser");

/*app.get('/hello', (req, res) => {
    res.send('Hello World!')
  })
  
  app.post('/create', (req, res) => {
    res.send('I am craeted')
  })
*/

app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
//app.use("/products", products); // whenever /products is used goto product module
app.use("/users", users);
app.use("/eom", employee);
app.listen(port, async () => {
  console.log(`listening at http://localhost:${port}`);
  await sequelize.sync({ force: false });
  await sequelize.authenticate();
  console.log("Database Connected");
});

////// error handling middleware
//   app.get('/', function(req, res){
//     //Create an error and pass it to the next function
//     var err = new Error("Something went wrong");
//     next(err);
//  });

//  /*
//   * other route handlers and middleware here
//   * ....
//   */

//  //An error handling middleware
//  app.use(function(err, req, res, next) {
//     res.status(500);
//     res.send("Oops, something went wrong.")
//  });
