const express = require("express");
const router = express.Router();
// var connection = require("../config/database");
// var bcrypt = require("bcryptjs");
const {
  registerUser,
  loginUser,
  allusers,
  eom,
} = require("../controllers/usersControllers.js");
const { finalvalidation } = require("../middleware/validation");
const { checkAuth } = require("../middleware/check-auth");

router.post(
  "/register",
  // (req, res, next) => {
  //   validationError = finalvalidation(req.body, [
  //     "email",
  //     "password",
  //     "name",
  //     "role",
  //   ]);
  //   if (validationError.length) {
  //     res.send(validationError);
  //   } else {
  //     next();
  //   }
  // },
  registerUser
);
router.post(
  "/login",
  // (req, res, next) => {
  //   validationError = finalvalidation(req.body, ["email", "password"]);
  //   if (validationError.length) {
  //     res.send(validationError);
  //   } else {
  //     next();
  //   }
  // },
  loginUser
);
router.get("/allusers", checkAuth, allusers); //checkAuth is middleware
router.get("/:id", checkAuth, eom);
module.exports = router;
