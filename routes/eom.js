const express = require("express");
const router = express.Router();
const { finalvalidation } = require("../middleware/validation");
const {
  Employee_of_month,
  All_employee_of_month,
  employeemonth,
} = require("../controllers/employyeController");
const { checkAdmin } = require("../middleware/check-admin");

const bodyParser = require("body-parser");
const { checkAuth } = require("../middleware/check-auth");

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.post(
  "/add",
  checkAuth,
  checkAdmin,
  // (req, res, next) => {
  //   validationError = finalvalidation(req.body, ["user_id", "year", "month"]);
  //   if (validationError.length) {
  //     res.send(validationError);
  //   } else {
  //     next();
  //   }
  // },
  Employee_of_month
);
router.get("/all", checkAuth, All_employee_of_month);
router.get("/eom", checkAuth, employeemonth);

module.exports = router;
